package fr.liberthei.survheillance.user.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import de.hdodenhof.circleimageview.CircleImageView;
import fr.liberthei.survheillance.R;
import fr.liberthei.survheillance.global.activity.LoginActivity;
import fr.liberthei.survheillance.user.adapter.FavRoomAdapter;
import fr.liberthei.survheillance.user.entity.Room;
import fr.liberthei.survheillance.user.manager.UserManager;
import fr.liberthei.survheillance.user.recyclerView.OnRecyclerItemClickListenerRoom;
import fr.liberthei.survheillance.user.tools.Singleton;

public class FavActivity extends AppCompatActivity implements OnRecyclerItemClickListenerRoom, BottomNavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "FavActivity";

    private FirebaseAuth mAuth;
    private StorageReference storageReference;
    private FirebaseStorage storage;
    private FirebaseFirestore db;
    private TextView noFavTextView;
    BottomNavigationView bottomNavigationView;
    RecyclerView recyclerViewRoom;
    private ImageView guillaume;
    private UserManager userManager = UserManager.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav);
        mAuth = FirebaseAuth.getInstance();
        initLayoutId();
        setRecyclerViewRoom();
        setStorage();
        getListItems();
        setupListeners();



    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }

    //Initialisation des variables présent dans le layout
    private void initLayoutId(){
        noFavTextView = findViewById(R.id.textViewNoFavFav);
        guillaume = findViewById(R.id.imageViewGuillaumeFav);
        Singleton.getInstance().clearFavRoomList(); bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.navigation_fav);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

    }

    public void setRecyclerViewRoom(){
        recyclerViewRoom = findViewById(R.id.recyclerViewRoomFav);
        recyclerViewRoom.setHasFixedSize(false);
        recyclerViewRoom.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewRoom.setAdapter(new FavRoomAdapter(this));
    }

    private void setStorage(){
        // Create a storage reference from our app
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        db=FirebaseFirestore.getInstance();
        Log.d(TAG,"Successfully set storage ref");
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.navigation_List:
                intent= new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.navigation_fav:
                intent= new Intent(this, FavActivity.class);
                startActivity(intent);
                return true;
        }
        return false;
    }


    private void setupListeners(){


    }

    private void getListItems() {
        db.collection("users")
                .document(mAuth.getUid())
                .collection("favorites")
                .get()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Pierre","8 e : "+e.getMessage());
                    }
                })
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Singleton.getInstance().setFavRoomList(document.toObject(Room.class));
                            }
                            if(Singleton.getInstance().getFavRoomList().size()==0){
                                noFavTextView.setVisibility(View.VISIBLE);
                                guillaume.setVisibility(View.VISIBLE);
                                recyclerViewRoom.getAdapter().notifyDataSetChanged();

                            }else{
                                noFavTextView.setVisibility(View.GONE);
                                guillaume.setVisibility(View.GONE);

                                recyclerViewRoom.getAdapter().notifyDataSetChanged();
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

    }



    @Override
    public void onRecyclerViewItemClick(View view, int position) {
        Intent intent = new Intent(this, RoomDetail.class);
        intent.putExtra("position",position);
        intent.putExtra("isFav",1);
        startActivity(intent);
    }
}
