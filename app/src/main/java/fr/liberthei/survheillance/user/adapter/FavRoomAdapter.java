package fr.liberthei.survheillance.user.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import fr.liberthei.survheillance.R;
import fr.liberthei.survheillance.user.entity.Room;
import fr.liberthei.survheillance.user.recyclerView.OnRecyclerItemClickListenerRoom;
import fr.liberthei.survheillance.user.tools.Singleton;

public class FavRoomAdapter extends RecyclerView.Adapter<FavRoomAdapter.ViewHolder> {
    public static OnRecyclerItemClickListenerRoom onRecyclerItemClickListenerRoom;

    public FavRoomAdapter(OnRecyclerItemClickListenerRoom _onRecyclerItemClickListener) {
        onRecyclerItemClickListenerRoom = _onRecyclerItemClickListener;
    }

    @NonNull
    @Override
    public FavRoomAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_room, parent, false);
        return new FavRoomAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavRoomAdapter.ViewHolder holder, int position) {


        holder.setPlace(Singleton.getInstance().getFavRoomList().get(position));


    }

    @Override
    public int getItemCount() {

        return Singleton.getInstance().getFavRoomList().size();

    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        //private TextView textview_placeId;
        private TextView textviewName;



        public ViewHolder(View view) {
            super(view);
            textviewName = view.findViewById(R.id.textViewNameGrid);


            view.setOnClickListener(this);


        }

        public void setPlace(Room room) {

            textviewName.setText(room.getName());



        }


        @Override
        public void onClick(View view) {
            onRecyclerItemClickListenerRoom.onRecyclerViewItemClick(view, getAdapterPosition());
        }
    }
}

