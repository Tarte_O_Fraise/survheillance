package fr.liberthei.survheillance.user.entity;

public class User {
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private Boolean admin;



    public User(){}

    public User(String id_, String firstName_, String lastName_, String email_,boolean admin){
        this.id=id_;
        this.firstName=firstName_;
        this.lastName=lastName_;
        this.email=email_;
        this.admin = admin;

    }

    public User(String id_, String firstName_, String lastName_, String email_){
        this.id=id_;
        this.firstName=firstName_;
        this.lastName=lastName_;
        this.email=email_;
        this.admin = false;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getAdmin() {
        return this.admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }
}
