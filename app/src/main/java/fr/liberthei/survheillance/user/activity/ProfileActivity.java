package fr.liberthei.survheillance.user.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import fr.liberthei.survheillance.R;
import fr.liberthei.survheillance.user.entity.User;
import fr.liberthei.survheillance.global.activity.ChangePasswordActivity;
import fr.liberthei.survheillance.global.activity.LoginActivity;
import fr.liberthei.survheillance.user.manager.UserManager;
import fr.liberthei.survheillance.user.tools.Singleton;

public class ProfileActivity extends AppCompatActivity {
    private static final String TAG = "ProfilActivity";
    private Button btnLogOut,btnDelete, btnChangePassword , btnUpload;
    private ImageButton btnBack;
    private TextView nameTextView, mailTextView;
    private ImageView imageViewProfile;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private StorageReference storageReference;
    private FirebaseStorage storage;
    private DatabaseReference databaseReference;
    private DocumentReference documentReference;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private UserManager userManager = UserManager.getInstance();
    private Uri imageUri;
    private User userConnected;
    private static final int PICK_IMAGE =1;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        mAuth = FirebaseAuth.getInstance();
        setStorage();
        initLayoutId();
        setupListeners();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }




    //Initialisation des variables présent dans le layout
    private void initLayoutId(){
        btnLogOut=findViewById(R.id.buttonLogOutProfile);
        btnDelete=findViewById(R.id.buttonDeleteProfile);
        btnChangePassword=findViewById(R.id.buttonChangePasswordProfile);
        btnUpload = findViewById(R.id.buttonUploadProfile);
        btnBack = findViewById(R.id.buttonBackArrowProfile);
        mailTextView = findViewById(R.id.textViewEmailProfile);
        nameTextView = findViewById(R.id.textViewNomPrenomProfile);
        imageViewProfile = findViewById(R.id.imageViewProfilProfile);
        getUserInformation();
    }

    private void setStorage(){
        // Create a storage reference from our app
        documentReference = db.collection("users").document(mAuth.getCurrentUser().getUid());
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        databaseReference = database.getReference("Users");

        Log.d(TAG,"Successfully set storage ref");
    }

    private void getUserInformation(){


        DocumentReference docRef = db.collection("users").document(mAuth.getCurrentUser().getUid());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        userConnected = document.toObject(User.class);
                        Singleton.getInstance().createUser(userConnected);
                        mailTextView.setText(userConnected.getEmail());
                        nameTextView.setText(userConnected.getFirstName()+" "+userConnected.getLastName());
                        mailTextView.setText(Singleton.getInstance().getUser().getEmail());
                        nameTextView.setText(Singleton.getInstance().getUser().getFirstName()+" "+Singleton.getInstance().getUser().getLastName());
                        downloadImage();
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });






    }



    private void setupListeners(){
        // Sign out button
        btnLogOut.setOnClickListener(view -> {
            userManager.signOut();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            });

        btnBack.setOnClickListener(view -> {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        });

        btnDelete.setOnClickListener(view -> {
            userManager.deleteUser();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        });

        btnChangePassword.setOnClickListener(view -> {
            Intent intent = new Intent(this, ChangePasswordActivity.class);
            startActivity(intent);
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageUri != null){
                    uploadPicture();
                }
            }
        });

        imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent,1);


            }
        });

    }


    private void downloadImage(){
        storageReference.child("images/"+mAuth.getCurrentUser().getUid()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(imageViewProfile).load(uri).into(imageViewProfile);
                //Picasso.get().load(uri).rotate(rotate).into(imageViewProfile);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(ProfileActivity.this,"Failed Image download",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode ==PICK_IMAGE || resultCode == RESULT_OK || data != null || data.getData() != null){
            imageUri = data.getData();
            Picasso.get().load(imageUri).into(imageViewProfile);
        }

        }catch (Exception e){
            Toast.makeText(this, "Error"+e, Toast.LENGTH_SHORT).show();
        }


        }



    private void uploadPicture(){

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Uploading image...");
        pd.show();

        StorageReference riversRef = storageReference.child("images/"+mAuth.getCurrentUser().getUid());
        riversRef.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        pd.dismiss();
                        Toast.makeText(ProfileActivity.this,"Image uploaded",Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(ProfileActivity.this,"Failed Image uploaded",Toast.LENGTH_SHORT).show();
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                double progressPercent = (100.00 * snapshot.getBytesTransferred() / snapshot.getTotalByteCount());
                pd.setMessage("Percentage: "+(int) progressPercent+" %");
            }
        });
    }





}