package fr.liberthei.survheillance.user.recyclerView;

import android.view.View;

public interface OnRecyclerItemClickListenerRoom {

    void onRecyclerViewItemClick(View view, int position);

}

