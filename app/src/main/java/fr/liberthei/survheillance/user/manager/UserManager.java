package fr.liberthei.survheillance.user.manager;

import com.google.firebase.auth.FirebaseUser;

import fr.liberthei.survheillance.user.repository.UserRepository;

public class UserManager {
    private static final String TAG = "UserManager";
    private static volatile UserManager instance;
    private UserRepository userRepository;

    private UserManager() {
        userRepository = UserRepository.getInstance();
    }

    public static UserManager getInstance() {
        UserManager result = instance;
        if (result != null) {
            return result;
        }
        synchronized(UserRepository.class) {
            if (instance == null) {
                instance = new UserManager();
            }
            return instance;
        }
    }


    public FirebaseUser getCurrentUser(){
        return userRepository.getCurrentUser();
    }

    public Boolean isCurrentUserLogged(){
        return (this.getCurrentUser() != null);
    }

    public void signOut(){
         userRepository.signOut();
    }


    public void deleteUser(){
        userRepository.deleteUser();
    }

    public void changePassword(String newPassword){
        userRepository.changePassword(newPassword);
    }


}
