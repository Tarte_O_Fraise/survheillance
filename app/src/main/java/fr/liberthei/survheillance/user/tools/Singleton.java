package fr.liberthei.survheillance.user.tools;

import java.util.ArrayList;

import fr.liberthei.survheillance.user.entity.Room;
import fr.liberthei.survheillance.user.entity.User;

public class Singleton {


    private static final Singleton instance = new Singleton();
    private static final String TAG = "Singleton";
    private static final ArrayList<Room> roomList = new ArrayList<>();
    private static final ArrayList<Room> favRoomList = new ArrayList<>();
    private User user =null;
    private static ArrayList<Room> actualList = new ArrayList<>();


    private Singleton()
    {


    }

    public static final Singleton getInstance()
    {
        return instance;
    }

    public void createBDD(){
        roomList.clear();
        roomList.add(new Room("1","T145","HEI",100,50));
        roomList.add(new Room("2","T245","HEI",100,10));
        roomList.add(new Room("3","S102","HEI",90,2));
        roomList.add(new Room("4","B107","ISEN",50,45));
        roomList.add(new Room("5","T115","HEI",100,67));
        roomList.add(new Room("6","T215","HEI",100,10));
        roomList.add(new Room("7","S202","HEI",90,2));
        roomList.add(new Room("8","C107","ISEN",50,45));
        roomList.add(new Room("9","T315","HEI",100,67));
        roomList.add(new Room("10","T315","HEI",100,10));
        roomList.add(new Room("11","S101","HEI",90,2));
        roomList.add(new Room("12","A107","ISEN",50,45));

    }


    public void createUser(User user){
        this.user = new User(user.getId(),user.getFirstName(), user.getLastName(), user.getEmail(), user.getAdmin());
    }

    public ArrayList<Room> getRoomList(){
        return roomList;
    }
    public ArrayList<Room> getFavRoomList(){return  favRoomList;}
    public ArrayList<Room> getActualList(){return  actualList;}
    public void setFavRoomList(Room room){
        favRoomList.add(room);
    }
    public void clearFavRoomList(){
        favRoomList.clear();
    }
    public User getUser(){
        return user;
    }
    public void changeActualList(ArrayList<Room> actualList){
        this.actualList = actualList;
    }



}
