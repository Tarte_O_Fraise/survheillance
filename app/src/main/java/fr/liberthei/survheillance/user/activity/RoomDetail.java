package fr.liberthei.survheillance.user.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

import fr.liberthei.survheillance.R;
import fr.liberthei.survheillance.user.entity.Room;
import fr.liberthei.survheillance.global.activity.LoginActivity;
import fr.liberthei.survheillance.user.manager.UserManager;
import fr.liberthei.survheillance.user.tools.Singleton;

public class RoomDetail extends AppCompatActivity {
    private static final String TAG = "RoomDetail";


    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private UserManager userManager = UserManager.getInstance();
    private PieChart pieChart;
    private TextView nameTextView;
    private TextView buildingTextView;
    private ImageButton backBtn;
    private ImageButton favBtn;
    private Room room;
    private int isFav;
    private int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_detail);
        mAuth = FirebaseAuth.getInstance();
        initLayoutId();
        setupListeners();
        setText();
        setupPieChart();
        loadPieChartData();
        favVerif();

    }

    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }

    //Initialisation des variables présent dans le layout
    private void initLayoutId(){
        nameTextView = findViewById(R.id.textViewNameRoomDetail);
        buildingTextView = findViewById(R.id.textViewBuildingRoomDetail);
        backBtn = findViewById(R.id.buttonBackArrowRoomDetail);
        pieChart = findViewById(R.id.pieChartGraphRoomDetail);
        favBtn = findViewById(R.id.buttonFavorisRoomDetail);
        position = getIntent().getIntExtra("position",0);
        isFav = getIntent().getIntExtra("isFav",0);
        if(isFav == 1){
            room = Singleton.getInstance().getFavRoomList().get(position);

        }else{
            room = Singleton.getInstance().getActualList().get(position);

        }


    }

    private void setText(){
        nameTextView.setText(room.getName());
        buildingTextView.setText(room.getBatiment());

    }

    private void setupListeners(){

        if(isFav==1){
            backBtn.setOnClickListener(view -> {
                Intent intent = new Intent(this, FavActivity.class);
                startActivity(intent);
            });
        }else{
            backBtn.setOnClickListener(view -> {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            });
        }



    }

    private void favVerif(){
        db=FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("users").document(mAuth.getUid())
                .collection("favorites").document(room.getId());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        favBtn.setOnClickListener(view -> {deleteFavFromDb();});
                        favBtn.setImageResource(R.drawable.ic_baseline_favorite_24_red);
                    } else {
                        Log.d(TAG, "No such document");
                        favBtn.setOnClickListener(view -> {addFavToDb();});
                        favBtn.setImageResource(R.drawable.ic_baseline_favorite_24);
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }

    private void addFavToDb(){
        String uid=mAuth.getUid();
        if(uid!=null) {
            db.collection("users").document(uid).collection("favorites")
                    .document(room.getId())
                    .set(room).addOnSuccessListener(new OnSuccessListener() {
                @Override
                public void onSuccess(Object o) {
                    Toast.makeText(RoomDetail.this, "Add Fav : Successful",Toast.LENGTH_SHORT).show();
                    favBtn.setOnClickListener(view -> {deleteFavFromDb();});
                    favBtn.setImageResource(R.drawable.ic_baseline_favorite_24_red);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(RoomDetail.this, "Add Fav : Failure",Toast.LENGTH_SHORT).show();
                }
            });

        }
    }

    private void deleteFavFromDb(){
        String uid=mAuth.getUid();
        if(uid!=null) {
            db.collection("users").document(uid).collection("favorites")
                    .document(room.getId())
                    .delete().addOnSuccessListener(new OnSuccessListener() {
                @Override
                public void onSuccess(Object o) {
                    Toast.makeText(RoomDetail.this, "Fav delete : successful",Toast.LENGTH_SHORT).show();
                    favBtn.setOnClickListener(view -> {addFavToDb();});
                    favBtn.setImageResource(R.drawable.ic_baseline_favorite_24);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(RoomDetail.this, "Fav delete : failure",Toast.LENGTH_SHORT).show();
                }
            });

        }
    }



    private void setupPieChart(){
        pieChart.setDrawHoleEnabled(true);
        pieChart.setUsePercentValues(true);
        pieChart.setEntryLabelTextSize(12);
        pieChart.setCenterText("Places restantes : "+Integer.toString(room.getNumberSeatAvailable()-room.getNumberSeatOccuped()));
        pieChart.setCenterTextSize(24);
        pieChart.getDescription().setEnabled(false);

        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setEnabled(true);
    }

    private void loadPieChartData(){
        ArrayList<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(room.getNumberSeatAvailable()-room.getNumberSeatOccuped(),"Seat Available"));
        entries.add(new PieEntry(room.getNumberSeatOccuped(),"Seat Occuped"));


        ArrayList<Integer>colors = new ArrayList<>();
        colors.add(getColor(R.color.greenGraph));
        colors.add(getColor(R.color.redGraph));


        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setDrawValues(true);
        data.setValueFormatter(new PercentFormatter(pieChart));
        data.setValueTextSize(12f);

        pieChart.setData(data);
        pieChart.invalidate();

        pieChart.animateY(1400, Easing.EaseInOutQuad);
    }


}
