package fr.liberthei.survheillance.user.entity;

public class Room {
    private String id;
    private String name;
    private String batiment;
    private int numberSeatAvailable;
    private int numberSeatOccuped;

    public Room(){

    }

    public Room (String id, String name, String batiment, int numberSeatAvailable, int numberSeatOccuped){
        this.id=id;
        this.name=name;
        this.batiment=batiment;
        this.numberSeatAvailable=numberSeatAvailable;
        this.numberSeatOccuped=numberSeatOccuped;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getBatiment(){
        return batiment;
    }

    public void setBatiment(String batiment){
        this.batiment = batiment;
    }

    public int getNumberSeatAvailable(){
        return numberSeatAvailable;
    }

    public void setNumberSeatAvailable(int numberSeatAvailable){
        this.numberSeatAvailable = numberSeatAvailable;
    }

    public int getNumberSeatOccuped(){
        return numberSeatOccuped;
    }

    public void setNumberSeatOccuped(int numberSeatOccuped){
        this.numberSeatOccuped=numberSeatOccuped;
    }

}
