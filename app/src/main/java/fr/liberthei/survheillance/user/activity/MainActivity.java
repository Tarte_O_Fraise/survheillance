package fr.liberthei.survheillance.user.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import fr.liberthei.survheillance.R;
import fr.liberthei.survheillance.user.adapter.RoomAdapter;
import fr.liberthei.survheillance.global.activity.LoginActivity;
import fr.liberthei.survheillance.user.entity.Room;
import fr.liberthei.survheillance.user.manager.UserManager;
import fr.liberthei.survheillance.user.recyclerView.OnRecyclerItemClickListenerRoom;
import fr.liberthei.survheillance.user.tools.Singleton;

public class MainActivity extends AppCompatActivity implements OnRecyclerItemClickListenerRoom, BottomNavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "MainActivity";

    private FirebaseAuth mAuth;
    private StorageReference storageReference;
    private CircleImageView imageViewProfile;
    private FirebaseStorage storage;
    private ImageButton favBtn;
    private ArrayList<Room> roomSearch;
    BottomNavigationView bottomNavigationView;
    RecyclerView recyclerViewRoom;
    RoomAdapter adapter;
    Toolbar toolbar;
    private UserManager userManager = UserManager.getInstance();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_user);
        Singleton.getInstance().createBDD();
        mAuth = FirebaseAuth.getInstance();
        initLayoutId();
        setStorage();
        downloadImage();
        setupListeners();
        setRecyclerViewRoom();



    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }

    //Initialisation des variables présent dans le layout
    private void initLayoutId(){
        imageViewProfile = findViewById(R.id.imageViewProfilFav);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);


    }

    public void setRecyclerViewRoom(){
        adapter = new RoomAdapter(this,getApplicationContext(),Singleton.getInstance().getRoomList() );
        recyclerViewRoom = findViewById(R.id.recyclerViewRoomMain);
        recyclerViewRoom.setHasFixedSize(false);

        // setting layout manager
        // to our recycler view.
        recyclerViewRoom.setLayoutManager(new GridLayoutManager(this, 2));

        // setting adapter to
        // our recycler view.
        recyclerViewRoom.setAdapter(adapter);


    }

    private void setStorage(){
        // Create a storage reference from our app
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        Log.d(TAG,"Successfully set storage ref");
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.navigation_List:
                 intent= new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.navigation_fav:
                 intent= new Intent(this, FavActivity.class);
                startActivity(intent);
                return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // below line is to get our inflater
        MenuInflater inflater = getMenuInflater();

        // inside inflater we are inflating our menu file.
        inflater.inflate(R.menu.menu_top, menu);

        // below line is to get our menu item.
        MenuItem searchItem = menu.findItem(R.id.itemSearchMenuTop);

        // getting search view of our item.
        SearchView searchView = (SearchView) searchItem.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return true;
            }
        });
        return true;
    }


    private void filter(String newText){
        ArrayList<Room> filteredList = new ArrayList<>();
        for(Room item : Singleton.getInstance().getRoomList()){
            if(item.getName().toLowerCase().contains(newText.toLowerCase())){
                filteredList.add(item);
            }
        }
        recyclerViewRoom.setAdapter(new RoomAdapter(this,getApplicationContext(),filteredList ));
    }

    private void downloadImage(){
        storageReference.child("images/"+mAuth.getCurrentUser().getUid()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(imageViewProfile).load(uri).into(imageViewProfile);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(MainActivity.this,"Failed Image download",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupListeners(){

        imageViewProfile.setOnClickListener(view -> {
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        });


    }


    @Override
    public void onRecyclerViewItemClick(View view, int position) {
        Intent intent = new Intent(this, RoomDetail.class);
        intent.putExtra("position",position);
        startActivity(intent);
    }
}