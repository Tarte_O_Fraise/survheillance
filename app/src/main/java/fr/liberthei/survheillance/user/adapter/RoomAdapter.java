package fr.liberthei.survheillance.user.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import fr.liberthei.survheillance.R;
import fr.liberthei.survheillance.user.entity.Room;
import fr.liberthei.survheillance.user.recyclerView.OnRecyclerItemClickListenerRoom;
import fr.liberthei.survheillance.user.tools.Singleton;


public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.ViewHolder>{
    public static OnRecyclerItemClickListenerRoom onRecyclerItemClickListenerRoom;
    private Context context;
    private ArrayList<Room> roomArrayList;

    public RoomAdapter( OnRecyclerItemClickListenerRoom _onRecyclerItemClickListener, Context context, ArrayList<Room> roomArrayList){
        onRecyclerItemClickListenerRoom = _onRecyclerItemClickListener;
        this.roomArrayList=roomArrayList;
        Singleton.getInstance().changeActualList(this.roomArrayList);
    }



    @NonNull
    @Override
    public RoomAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_room,parent,false);
        return new RoomAdapter.ViewHolder(view,context);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomAdapter.ViewHolder holder, int position) {


        holder.setPlace(roomArrayList.get(position));



    }



    @Override
    public int getItemCount() {

        return roomArrayList.size();

    }



    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        //private TextView textview_placeId;
        private TextView textviewName;
        private TextView textviewSeatAvailable;
        private TextView textviewSeatOccuped;
        private TextView textviewBuilidng;
        private GridLayout gridLayout;
        private Context context;




        public ViewHolder(View view,Context context){
            super(view);
            gridLayout = view.findViewById(R.id.line_layout);
            textviewName = view.findViewById(R.id.textViewNameGrid);
            //textviewSeatAvailable = view.findViewById(R.id.textViewSeatAvailableLine);
            //textviewSeatOccuped = view.findViewById(R.id.textViewSeatOccupedLine);
            //textviewBuilidng = view.findViewById(R.id.textViewBuildingLine);

            view.setOnClickListener(this);


        }

        public void setPlace(Room room){

            textviewName.setText(room.getName());
            Double percentage=Double.valueOf(room.getNumberSeatOccuped())/Double.valueOf(room.getNumberSeatAvailable());

            Log.d("Room1",Double.toString(room.getNumberSeatOccuped()));
            Log.d("Room2",Double.toString(room.getNumberSeatAvailable()));
            Log.d("percentage",Double.toString(percentage));
            if(percentage>0.75){
                gridLayout.setBackgroundResource(R.drawable.bordure_rouge);
            }else if(percentage>0.25 && percentage<0.75){
                gridLayout.setBackgroundResource(R.drawable.bordure_corail);
                Log.d("couleur","gris");
            }

        }


        @Override
        public void onClick(View view) {
            onRecyclerItemClickListenerRoom.onRecyclerViewItemClick(view,getAdapterPosition());
        }
    }

}
