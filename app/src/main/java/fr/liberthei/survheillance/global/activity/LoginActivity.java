package fr.liberthei.survheillance.global.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import fr.liberthei.survheillance.R;
import fr.liberthei.survheillance.user.activity.MainActivity;
import fr.liberthei.survheillance.user.manager.UserManager;

public class LoginActivity extends AppCompatActivity {
    //Tag pour les logs
    private static final String TAG = "LoginActivity";

    //Déclaration du Singleton
    private UserManager userManager = UserManager.getInstance();


    //Firebase
    private FirebaseAuth mAuth;

    //Déclaration des éléments visuels
    private Button btnSignIn, btnSignUp, btnForgotPassword;
    private EditText editTextEmail, editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        //initialisation des éléments visuels
        initLayoutId();

        //Gestion des clics sur les boutons
        setupListeners();


    }

    @Override
    public void onStart() {
        super.onStart();
        //On vérifie si quelqu'un est déjà connecté
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(userManager.isCurrentUserLogged()){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

    }




    //Initialisation des variables présent dans le layout
    private void initLayoutId(){
        editTextEmail=findViewById(R.id.editTextEmailLogin);
        editTextPassword=findViewById(R.id.editTextPasswordLogin);
        btnSignIn=findViewById(R.id.buttonSignInButtonLogin);
        btnSignUp=findViewById(R.id.buttonSignUpButtonLogin);
        btnForgotPassword=findViewById(R.id.buttonForgotPassword);
    }

    private void setupListeners(){
        btnSignIn.setOnClickListener((view -> {signIn();}));
        btnSignUp.setOnClickListener((view -> {signUp();}));
        btnForgotPassword.setOnClickListener((view -> {forgotPassword();}));
    }

    private void signUp(){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void signIn() {
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();

        Log.d(TAG, "signIn:" + email);
        if (!validateForm(email, password)) {
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent intent=new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                    }
                });
    }

    private void forgotPassword(){
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    private boolean validateForm(String email, String password) {
        boolean valid = true;
        if(email.isEmpty() || !email.contains("@")){
            showError(editTextEmail, "Invalid email");
            valid=false;
        }
        if(password.isEmpty() || password.length()<7){
            showError(editTextPassword,"Password must be 7 characters");
            valid=false;
        }
        return valid;
    }

    private void showError(EditText input, String string){
        input.setError(string);
        input.requestFocus();
    }

}
