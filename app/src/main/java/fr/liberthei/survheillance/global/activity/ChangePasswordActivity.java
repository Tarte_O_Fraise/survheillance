package fr.liberthei.survheillance.global.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import fr.liberthei.survheillance.R;
import fr.liberthei.survheillance.user.activity.ProfileActivity;
import fr.liberthei.survheillance.user.manager.UserManager;

public class ChangePasswordActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    private UserManager userManager = UserManager.getInstance();
    private FirebaseAuth mAuth;

    private Button btnChangePassword;
    private EditText editTextPassword, editTextConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        initLayoutId();

        btnChangePassword.setOnClickListener((view -> {
            changePassword();}));

    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

    }

    //Initialisation des variables présent dans le layout
    private void initLayoutId(){
        editTextConfirmPassword=findViewById(R.id.editTextConfirmPasswordPassword);
        editTextPassword=findViewById(R.id.editTextPasswordPassword);
        btnChangePassword=findViewById(R.id.buttonChangePasswordPassword);

    }



    private void changePassword() {
        String password = editTextPassword.getText().toString();
        String confirmPassword = editTextConfirmPassword.getText().toString();

        if (!validateForm(password, confirmPassword)) {
            return;
        }

        userManager.changePassword(password);
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);


    }

    private boolean validateForm(String password, String confirmPassword) {
        boolean valid = true;

        if(password.isEmpty() || password.length()<7){
            showError(editTextPassword,"Password must be 7 characters");
            valid=false;
        } else if(confirmPassword.isEmpty() || !confirmPassword.equals(password)){
            showError(editTextConfirmPassword, "Password doesn't match");
        }
        return valid;
    }

    private void showError(EditText input, String string){
        input.setError(string);
        input.requestFocus();
    }

}
