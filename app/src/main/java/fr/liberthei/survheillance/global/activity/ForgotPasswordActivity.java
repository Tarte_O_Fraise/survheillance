package fr.liberthei.survheillance.global.activity;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import fr.liberthei.survheillance.R;
import fr.liberthei.survheillance.user.activity.MainActivity;
import fr.liberthei.survheillance.user.manager.UserManager;

public class ForgotPasswordActivity extends AppCompatActivity {
    private Button submitPassword;
    private EditText editTextEmailLogin;

    //Déclaration du Singleton
    private UserManager userManager = UserManager.getInstance();


    //Firebase
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mAuth = FirebaseAuth.getInstance();
        initLayoutId();
        submitPassword.setOnClickListener((view -> {
            ReinitPassword();}));

    }


    @Override
    public void onStart() {
        super.onStart();
        //On vérifie si quelqu'un est déjà connecté
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(userManager.isCurrentUserLogged()){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

    }

    //Initialisation des variables présent dans le layout
    private void initLayoutId(){
        editTextEmailLogin = findViewById(R.id.editTextEmailForgotPassword);
        submitPassword = findViewById(R.id.buttonSubmitForgotPassword);

    }



    private void ReinitPassword(){
        String email = editTextEmailLogin.getText().toString().trim();
        if (email.isEmpty()){
            Toast.makeText(
                    this, "Please enter email address", Toast.LENGTH_SHORT
            ).show();
            Log.d(TAG, "Please enter email address");
        }else{
            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(ForgotPasswordActivity.this, "Email sent successfully to reset your password",
                                        Toast.LENGTH_LONG).show();
                                Log.d(TAG, "Email sent successfully to reset your password");
                                finish();
                            }else{
                                Toast.makeText(ForgotPasswordActivity.this, task.getException().getLocalizedMessage().toString(),
                                        Toast.LENGTH_LONG).show();
                                Log.d(TAG, "Email sent successfully to reset your password");
                            }
                        }

                    });
        }
    }
}