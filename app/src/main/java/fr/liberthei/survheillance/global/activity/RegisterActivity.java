package fr.liberthei.survheillance.global.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import fr.liberthei.survheillance.R;
import fr.liberthei.survheillance.user.activity.MainActivity;
import fr.liberthei.survheillance.user.activity.ProfileActivity;
import fr.liberthei.survheillance.user.entity.User;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private StorageReference storageReference;
    private FirebaseStorage storage;
    private static final String TAG = "RegisterActivity";
    private EditText editTextEmail, editTextPassword, editTextConfirmPassword, editTextFirstName, editTextLastName;
    private Button btnRegister;
    private ImageView imageViewProfile;
    private static final int PICK_IMAGE =1;
    private Uri imageUri;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // Initalisation de l'authentification avec firebase
        mAuth = FirebaseAuth.getInstance();
        db=FirebaseFirestore.getInstance();

        initLayoutId();
        setupListeners();

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }




    //Initialisation des variables présent dans le layout
    private void initLayoutId(){
        editTextEmail=findViewById(R.id.editTextEmailRegister);
        editTextPassword=findViewById(R.id.editTextPasswordRegister);
        editTextLastName=findViewById(R.id.editTextLastNameRegister);
        editTextFirstName=findViewById(R.id.editTextFirstNameRegister);
        imageViewProfile=findViewById(R.id.imageViewProfilRegister);
        editTextConfirmPassword=findViewById(R.id.editTextConfirmPasswordRegister);
        btnRegister=findViewById(R.id.buttonSignUpRegister);

    }

    private void setupListeners(){

        btnRegister.setOnClickListener((view -> {createAccount();}));

        imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent,1);
            }
        });

    }

    private void createAccount(){
        String email=editTextEmail.getText().toString();
        String password=editTextPassword.getText().toString();
        String lastName=editTextLastName.getText().toString();
        String firstName=editTextFirstName.getText().toString();
        String confirmPassword=editTextConfirmPassword.getText().toString();;

        if(firstName.isEmpty() || firstName.matches("[^a-z]")){
            showError(editTextFirstName, "Strange first name");
        }
        else if(lastName.isEmpty() || lastName.matches("[^a-z]")){
            showError(editTextLastName, "Strange last name");
        }
        else if(email.isEmpty() || !email.contains("@")){
            showError(editTextEmail, "Invalid email");
        }
        else if(password.isEmpty() || password.length()<7){
            showError(editTextPassword,"Password must be 7 characters");
        }
        else if(confirmPassword.isEmpty() || !confirmPassword.equals(password)){
            showError(editTextConfirmPassword, "Password doesn't match");
        }
        else{
            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Log.d(TAG, "createUserWithEmail:success");
                        String id = mAuth.getUid();
                        Log.d(TAG, id);
                        User newUser = new User(id,firstName,lastName,email);
                        addUserToDb(newUser);
                        addPhotoToDb(id);
                    }else{
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        Toast.makeText(RegisterActivity.this, "Sign up failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }

    private void addUserToDb(User newUser){
        db.collection("users")
                .document(newUser.getId()).set(newUser)
                .addOnSuccessListener(new OnSuccessListener() {
                    @Override
                    public void onSuccess(Object o) {
                        Toast.makeText(RegisterActivity.this, "Successful",Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(RegisterActivity.this, "Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void addPhotoToDb(String id){
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        StorageReference riversRef = storageReference.child("images/"+id);
        if(imageUri!=null){
            riversRef.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(RegisterActivity.this,"Image uploaded",Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(RegisterActivity.this, ProfileActivity.class);
                            startActivity(intent);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(RegisterActivity.this,"Failed Image uploaded",Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            Intent intent=new Intent(RegisterActivity.this, ProfileActivity.class);
            startActivity(intent);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode ==PICK_IMAGE || resultCode == RESULT_OK || data != null || data.getData() != null){
                imageUri = data.getData();
                //simageViewProfile.setImageURI(imageUri);


                Picasso.get().load(imageUri).into(imageViewProfile);
            }

        }catch (Exception e){
            Toast.makeText(this, "Error"+e, Toast.LENGTH_SHORT).show();
        }
    }



    private void showError(EditText input, String string){
        input.setError(string);
        input.requestFocus();
    }

}
